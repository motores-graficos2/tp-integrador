using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    Vector2 MouseLook;
    Vector2 Smoothness;

    GameObject Player;

    public float Sensitivity;
    public float Smoothing;
    void Start()
    {
        Player = this.transform.parent.gameObject;
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(Sensitivity * Smoothing, Sensitivity * Smoothing));

        Smoothness.x = Mathf.Lerp(Smoothness.x, md.x, 1f / Smoothing);
        Smoothness.y = Mathf.Lerp(Smoothness.y, md.y, 1f / Smoothing);

        MouseLook += Smoothness;
        MouseLook.y = Mathf.Clamp(MouseLook.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-MouseLook.y, Vector3.right);
        Player.transform.localRotation = Quaternion.AngleAxis(MouseLook.x, Player.transform.up);
    }
}
