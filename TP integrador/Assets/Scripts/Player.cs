using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Speed = 10.0f;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float MovementVertical = Input.GetAxis("Vertical") * Speed;
        float MovementHorizontal = Input.GetAxis("Horizontal") * Speed;

        MovementVertical *= Time.deltaTime;
        MovementHorizontal *= Time.deltaTime;

        transform.Translate(MovementHorizontal, 0, MovementVertical);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
